module.exports = function(grunt) {
    grunt.initConfig({
        sass: {                              // Task
            dist: {                            // Target
                options: {                       // Target options
                    style: 'compressed',

                },
                files: {                         // Dictionary of files
                    'app/css/spongetest.min.css': 'app/css/spongetest.scss',       // 'destination': 'source'
                }
            }
        },
        browserify: {
            dist: {
                options: {
                    "transform": [["babelify", { "presets": ["es2015"] }]]                },
                files: {
                    "app/js/compiled.js": ["app/js/spongetest.js"]
                }
            }
        },    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks("grunt-browserify");
    grunt.registerTask('default', ['browserify','sass']);

};