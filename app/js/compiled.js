(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/*
 =====================================================

   _____                                _    _ _  __
  / ____|                              | |  | | |/ /
 | (___  _ __   ___  _ __   __ _  ___  | |  | | ' /
  \___ \| '_ \ / _ \| '_ \ / _` |/ _ \ | |  | |  <
  ____) | |_) | (_) | | | | (_| |  __/ | |__| | . \
 |_____/| .__/ \___/|_| |_|\__, |\___|  \____/|_|\_\
        | |                 __/ |
        |_|                |___/

 =====================================================
 SPONGE UK DEVELOPER TEST
 JSON parser and event handler
 =====================================================
*/
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ContentInstance = function ContentInstance(strDataLocation) {
  _classCallCheck(this, ContentInstance);

  var objContent = {},
      arrOnReady = [],
      blReady = false;

  /**
   * Get the JSON file
   */
  $.getJSON(strDataLocation, function (objResponse) {
    objContent = objResponse;
    blReady = true;

    /**
     * Execute all the ready functions once loaded
     */
    $.each(arrOnReady, function (intIndex, funDoOnReady) {
      funDoOnReady.call();
    });
  });

  /**
   * Register a function to execute once loaded
   */

  this.onReady = function (funDoOnReady) {
    if (blReady) {
      funDoOnReady.call();
    } else {
      arrOnReady.push(funDoOnReady);
    }
  };

  /**
   * Get an item from the content data
   * */

  this.getItem = function (intItem) {
    return objContent[intItem];
  };

  return this;
};

exports.default = ContentInstance;

/*
      ,'``.._   ,'``.
     :,--._:)\,:,._,.:       All Glory to
     :`--,''   :`...';\      the HYPNOTOAD!
      `,'       `---'  `.
      /                 :
     /                   \
   ,'                     :\.___,-.
  `...,---'``````-..._    |:       \
    (                 )   ;:    )   \  _,-.
     `.              (   //          `'    \
      :               `.//  )      )     , ;
    ,-|`.            _,'/       )    ) ,' ,'
   (  :`.`-..____..=:.-':     .     _,' ,'
    `,'\ ``--....-)='    `._,  \  ,') _ '``._
 _.-/ _ `.       (_)      /     )' ; / \ \`-.'
`--(   `-:`.     `' ___..'  _,-'   |/   `.)
    `-. `.`.``-----``--,  .'
      |/`.\`'        ,',');
          `         (/  (/
 */

},{}],2:[function(require,module,exports){
'use strict';

var _content = require('./lib/content.js');

var _content2 = _interopRequireDefault(_content);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

jQuery(function ($) {
	/**
  * A new instance of the content parser using the content JSON file
  */

	var resContent = new _content2.default('app/data/content.json');

	var populate = function populate(template, content, append) {

		try {
			var strTaskSource = $(template).html(),
			    resTasksTemplate = Handlebars.compile(strTaskSource),
			    strTasksHTML = resTasksTemplate(resContent.getItem(content));
			$(append).append(strTasksHTML);
		} catch (err) {
			console.log('Parsing ' + template + ' failed');
		}
	};

	/**
  * Register a Handlebars helper for the difficulty stars
  */
	Handlebars.registerHelper('difficulty', function (intStars) {
		var strHTMLStarsOut = '';

		for (var intStar = 0; intStar < intStars; intStar++) {
			strHTMLStarsOut += '<i class="fa fa-star"></i>';
		}

		for (var intBlankStar = intStars; intBlankStar < 5; intBlankStar++) {
			strHTMLStarsOut += '<i class="fa fa-star-o"></i>';
		}

		return strHTMLStarsOut;
	});

	/**
  * When the content file is ready, actually populate the content
  */
	resContent.onReady(function () {
		populate('#header-template', 'header', '#header');
		populate('#task-template', 'tasks', '#tasks');
		populate('#tab-template', 'tabs', '#tabs');
		populate('#content-template', 'content', '#content');
		populate('#documentation-template', 'docs', '#documentation');
	});
});
/*
 =====================================================

   _____                                _    _ _  __
  / ____|                              | |  | | |/ /
 | (___  _ __   ___  _ __   __ _  ___  | |  | | ' /
  \___ \| '_ \ / _ \| '_ \ / _` |/ _ \ | |  | |  <
  ____) | |_) | (_) | | | | (_| |  __/ | |__| | . \
 |_____/| .__/ \___/|_| |_|\__, |\___|  \____/|_|\_\
        | |                 __/ |
        |_|                |___/

=====================================================
 SPONGE UK DEVELOPER TEST
 Page-specific JS
=====================================================
*/


$("#tabs").on("click", '.tab-menu a', function (e) {
	e.preventDefault();
	var tab = $(this).attr('href');

	$('.tab-menu a').removeClass('active');
	$(this).addClass('active');

	$('.tab-content').removeClass('active');
	$(tab).addClass('active');
});

},{"./lib/content.js":1}]},{},[2]);
